from selenium.webdriver.common.by import By


class RegionalPageLocators:
    SIDE_OF_UI_CURRENCY = (By.XPATH, '//div[@class="currency"]')
    SIDE_OF_UI_COUNTRY = (By.XPATH, '//div[@class="country"]')
    CURRENCY_BUTTON = (By.XPATH, '//select[@name="currency_code"]/option[3]')
    COUNTRY_BUTTON = (By.XPATH, '//select[@name="country_code"]/option[53]')
    REGION_BUTTON = (By.XPATH, '//a[@class="fancybox-region"]')
    SAVE_BUTTON = (By.XPATH, '//button[@name="save"]')

    
from selenium.webdriver.common.by import By


class AccountPageLocators:
    BUTTON_LOGIN = (By.XPATH, '//button[@name="login"]')
    EDIT_ACCOUNT = (By.XPATH, '//div[@id="box-account"]/div/ul/li[3]/a')
    INPUT_EMAIL = (By.XPATH, '//input[@name="email"]')
    INPUT_PASS = (By.XPATH, '//input[@name="password"]')
    INPUT_NAME = (By.XPATH, '//input[@name="firstname"]')
    BUTTON_SUBMIT = (By.XPATH, '//button[@type="submit"]')


class BasketPageLocators:
    INPUT_EMAIL = (By.XPATH, '//input[@name="email"]')
    INPUT_PASS = (By.XPATH, '//input[@name="password"]')
    BUTTON_ADD_CART = (By.XPATH, '//button[@type="submit"]')
    BUTTON_CART = (By.XPATH, '//div[@id="cart"]/a[@class="link"]')
    SUM_PRICE = (By.XPATH, '//td[@class="sum"]')
    BUTTON_LOGIN = (By.XPATH, '//button[@name="login"]')
    SELECT_DUCK = (By.XPATH, '//a[@title="Blue Duck"][@class="link"]')
    COUNT = (By.XPATH, '//td[@style="text-align: center;"]')
    BUTTON_UPDATE = (By.XPATH, '//button[@name="update_cart_item"]')
    BUTTON_REMOVE = (By.XPATH, '//button[@name="remove_cart_item"]')
    EMPTY = (By.XPATH, '//em')

    COUNT_DUCK = (By.XPATH, '//input[@type="number"]')

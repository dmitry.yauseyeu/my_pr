from new_diplom.pages.base_page import BasePage
from new_diplom.locators.login_basket_page_locators import LoginBasketPageLocators


class LoginBasketPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login_email(self, arg="dmitry.yauseyeu@gmail.com"):
        button = self.driver.find_element(
            *LoginBasketPageLocators.INPUT_EMAIL)
        button.send_keys(arg)

    def login_pass(self, arg="123456"):
        button = self.driver.find_element(
            *LoginBasketPageLocators.INPUT_PASS)
        button.send_keys(arg)

    def login_button(self):
        button = self.driver.find_element(
            *LoginBasketPageLocators.BUTTON_LOGIN)
        button.click()

    def select_duck(self):
        button = self.driver.find_element(
            *LoginBasketPageLocators.SELECT_DUCK)
        button.click()

    def count_duck(self, arg):
        button = self.driver.find_element(
            *LoginBasketPageLocators.COUNT_DUCK)
        button.clear()
        button.send_keys(arg)

    def button_add_cart(self):
        button = self.driver.find_element(
            *LoginBasketPageLocators.BUTTON_ADD_CART)
        button.click()

    def open_cart(self):
        button = self.driver.find_element(
            *LoginBasketPageLocators.BUTTON_CART)
        button.click()

    def price(self):
        return self.driver.find_element(
            *LoginBasketPageLocators.PRICE)

    def count(self):
        return self.driver.find_element(
            *LoginBasketPageLocators.COUNT)

    def confirm(self):
        button = self.driver.find_element(
            *LoginBasketPageLocators.BUTTON_CONFIRM)
        button.click()

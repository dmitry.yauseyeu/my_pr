from new_diplom.pages.base_page import BasePage
from new_diplom.locators.region_page_locators import RegionalPageLocators


class RegionalPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def currency_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.CURRENCY_BUTTON)
        button.click()

    def country_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.COUNTRY_BUTTON)
        button.click()

    def open_regional_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.REGION_BUTTON)
        button.click()

    def ui_country(self):
        return self.driver.find_element(
            *RegionalPageLocators.SIDE_OF_UI_COUNTRY)

    def ui_currency(self):
        return self.driver.find_element(
            *RegionalPageLocators.SIDE_OF_UI_CURRENCY)

    def save_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.SAVE_BUTTON)
        button.click()

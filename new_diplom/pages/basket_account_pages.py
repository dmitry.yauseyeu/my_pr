from new_diplom.pages.base_page import BasePage
from new_diplom.locators.basket_account_pages_locators\
    import BasketPageLocators, AccountPageLocators


class BasketPage(BasePage):

    URL = "/litecart/en/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login_email(self, arg="dmitry.yauseyeu@gmail.com"):
        button = self.driver.find_element(
            *BasketPageLocators.INPUT_EMAIL)
        button.send_keys(arg)

    def login_pass(self, arg="123456"):
        button = self.driver.find_element(
            *BasketPageLocators.INPUT_PASS)
        button.send_keys(arg)

    def login_button(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_LOGIN)
        button.click()

    def select_duck(self):
        button = self.driver.find_element(
            *BasketPageLocators.SELECT_DUCK)
        button.click()

    def count_duck(self, arg):
        button = self.driver.find_element(
            *BasketPageLocators.COUNT_DUCK)
        button.clear()
        button.send_keys(arg)

    def button_add_cart(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_ADD_CART)
        button.click()

    def open_cart(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_CART)
        button.click()

    def sum_price(self):
        return self.driver.find_element(
            *BasketPageLocators.SUM_PRICE)

    def count(self):
        return self.driver.find_element(
            *BasketPageLocators.COUNT)

    def update(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_UPDATE)
        button.click()

    def remove(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_REMOVE)
        button.click()

    def empty(self):
        return self.driver.find_element(
            *BasketPageLocators.EMPTY)


class AccountPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login_email_account_page(self, arg="dmitry.yauseyeu@gmail.com"):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_EMAIL)
        button.send_keys(arg)

    def login_pass_account_page(self, arg="123456"):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_PASS)
        button.send_keys(arg)

    def login_button_account_page(self):
        button = self.driver.find_element(
            *AccountPageLocators.BUTTON_LOGIN)
        button.click()

    def edit_account_page(self):
        button = self.driver.find_element(
            *AccountPageLocators.EDIT_ACCOUNT)
        button.click()

    def input_name(self, arg):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_NAME)
        button.clear()
        button.send_keys(arg)

    def button_save(self):
        button = self.driver.find_element(
            *AccountPageLocators.BUTTON_SUBMIT)
        button.click()

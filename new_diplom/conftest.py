import pytest
from selenium import webdriver


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(executable_path="C:/Dim/chromedriver_win32/chromedriver")
    yield driver
    driver.quit()

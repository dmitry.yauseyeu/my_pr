import requests


def check_response(response):
    response.raise_for_status()
    return response


class RestHelper:
    def __init__(self, url):
        self.url = url
        self.session = requests.Session()

    def do_get(self, endpoint):
        response = self.session.get(self.url + endpoint)
        return check_response(response)

    def do_put(self, endpoint, **kwargs):
        response = self.session.put(self.url + endpoint, **kwargs)
        return check_response(response)

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response)

    def do_delete(self, endpoint):
        response = self.session.delete(self.url + endpoint)
        return check_response(response)
    
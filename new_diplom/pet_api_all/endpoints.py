class Endpoints:
    PET = "/pet"
    PET_ID = "/pet/{pet_id}"
    USER = "/user"
    NAME_USER = "/user/{username}"
    
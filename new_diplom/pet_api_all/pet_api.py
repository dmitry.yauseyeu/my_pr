from new_diplom.pet_api_all.helpers import RestHelper
from new_diplom.pet_api_all.endpoints import Endpoints


class PetStoreApi(RestHelper):
    def __init__(self):
        self.url = "https://petstore.swagger.io/"
        self.api_version = "v2"
        self.full_url = self.url + self.api_version
        super().__init__(self.full_url)

    def post_pet(self, id1, id2, name1, name2, string1,
                 id3, string2, available):
        data = {
            "id": id1,
            "category": {
                "id": id2,
                "name": name1
            },
            "name": name2,
            "photoUrls": [
                string1
            ],
            "tags": [
                {
                    "id": id3,
                    "name": string2
                }
            ],
            "status": available
        }
        return self.do_post(Endpoints.PET, json=data)

    def get_pet_id(self, pet_id):
        return self.do_get(Endpoints.PET_ID.format(pet_id=pet_id))

    def post_user(self, id_user, username, firstname, lastname,
                  email, password, phone, status):
        data = {
            "id": id_user,
            "username": username,
            "firstName": firstname,
            "lastName": lastname,
            "email": email,
            "password": password,
            "phone": phone,
            "userStatus": status
        }
        return self.do_post(Endpoints.USER, json=data)

    def delete_pet(self, pet_id):
        return self.do_delete(Endpoints.PET_ID.format(pet_id=pet_id))

    def get_user(self, username):
        return self.do_get(Endpoints.NAME_USER.format(username=username))

    def put_user(self, id_user, username, firstname, lastname, email,
                 password, phone, status):
        data = {
            "id": id_user,
            "username": username,
            "firstName": firstname,
            "lastName": lastname,
            "email": email,
            "password": password,
            "phone": phone,
            "userStatus": status
        }
        return self.do_put(Endpoints.NAME_USER, json=data)
    
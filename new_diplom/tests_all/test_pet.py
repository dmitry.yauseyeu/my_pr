import pytest
import allure
from new_diplom.pet_api_all.pet_api import PetStoreApi


response = "<Response [200]>"


@allure.story(" Add pet_API")
def test_add_pet():
    api = PetStoreApi()
    api.post_pet(70, 0, "", "Rasel", "", 0, "", "enabled")
    assert str(api.get_pet_id(70)) == response


@pytest.mark.xfail()
@allure.story("Delete pet_API")
def test_delete_pet():
    api = PetStoreApi()
    api.delete_pet(70)
    assert str(api.get_pet_id(70)) == response


@allure.story("Create user_API")
def test_create_user():
    api = PetStoreApi()
    api.post_user(3, "Tamara", "", "", "", "", "", 1)
    assert str(api.get_user("Tamara")) == response


@allure.story("Rename user_API")
def test_rename_user():
    api = PetStoreApi()
    api.put_user(4, "Dima.E", "", "", "", "", "", 2)
    assert str(api.get_user("Dima.E")) == response

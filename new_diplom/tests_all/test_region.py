import allure
from new_diplom.pages.region_page import RegionalPage


@allure.story("Task1=issue_1_currency")
def test_region_currency(driver):
    with allure.step('Open application'):
        button = RegionalPage(driver)
        button.open_page()
    with allure.step('Click on Regional settings'):
        button.open_regional_button()
        driver.implicitly_wait(10)
    with allure.step('Select currency'):
        button.currency_button()
    with allure.step('Save settings and check of UI side'):
        button.save_button()
        assert button.ui_currency().text == "EUR"


@allure.story("Task1=issue_1_country")
def test_region_country(driver):
    with allure.step('Open application'):
        button = RegionalPage(driver)
        button.open_page()
    with allure.step('Click on Regional settings'):
        button.open_regional_button()
        driver.implicitly_wait(10)
    with allure.step('Select country'):
        button.country_button()
    with allure.step('Save settings and check of UI side'):
        button.save_button()
        assert button.ui_country().text == "Cote D'Ivoire"

import allure
import time
from new_diplom.pages.basket_account_pages import BasketPage


@allure.story("Task2=issue_2_price")
def test_basket_price(driver):
    with allure.step('Open app'):
        basket = BasketPage(driver)
        basket.open_page()
    with allure.step('Logging'):
        basket.login_email()
        basket.login_pass()
        basket.login_button()
    with allure.step('Add duck into basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(1)
    with allure.step('Changing from 1 to 3 duck in basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(1)
    with allure.step('Check price is correct'):
        assert basket.sum_price().text == "$60.00"


@allure.story("Task2=issue_2_count")
def test_basket_count(driver):
    with allure.step('Open application'):
        basket = BasketPage(driver)
        basket.open_page()
        time.sleep(1)
    with allure.step('Logging'):
        basket.login_email()
        time.sleep(1)
        basket.login_pass()
        basket.login_button()
    with allure.step('Add duck into basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(1)
    with allure.step('Changing from 1 to 3 duck into basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(1)
    with allure.step('Check for added ducks is correct'):
        assert basket.count().text == "3"


@allure.story("Task2=issue_2_remove")
def test_basket_remove(driver):
    with allure.step('Open application'):
        basket = BasketPage(driver)
        basket.open_page()
    with allure.step('Logging into'):
        basket.login_email()
        basket.login_pass()
        basket.login_button()
    with allure.step('Add duck into basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(2)
    with allure.step('Changing from 1 to 3 duck into basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(2)
    with allure.step('Deleting and checking for empty basket'):
        basket.remove()
        driver.implicitly_wait(12)
        assert basket.empty().text == "There are no items in your cart."

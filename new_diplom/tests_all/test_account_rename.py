import allure
import mysql.connector
from new_diplom.pages.basket_account_pages import AccountPage


@allure.story("Task2=issue_1")
def test_account_rename_db(driver):
    with allure.step('Open application'):
        account = AccountPage(driver)
        account.open_page()
    with allure.step('Logging into '):
        account.login_email_account_page()
        account.login_pass_account_page()
        account.login_button_account_page()
    with allure.step('Open edit mode for rename of account '):
        account.edit_account_page()
        account.input_name("MAX")
        account.button_save()
    with allure.step('Check DB'):
        db = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="",
            database="litecart"
        )
        cursor = db.cursor()
        query = "SELECT firstname FROM lc_customers where firstname='MAX'"
        cursor.execute(query)
        assert cursor.fetchall()[0][0] == "MAX"
        cursor.execute(query)
        print(cursor.fetchall())
        db.close()

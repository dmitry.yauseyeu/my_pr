import allure
import time
import mysql.connector
from new_diplom.pages.login_basket_page import LoginBasketPage


@allure.story("Task1=issue_2_price")
def test_login_basket_price(driver):
    with allure.step('Open application'):
        login = LoginBasketPage(driver)
        login.open_page()
    with allure.step('Logging'):
        login.login_email()
        login.login_pass()
        login.login_button()
    with allure.step('Add 3 ducks into basket'):
        login.select_duck()
        login.count_duck("3")
        login.button_add_cart()
        time.sleep(2)
    with allure.step('Check of price is correct'):
        login.open_cart()
        assert login.price().text == "$20.00"
        login.confirm()


@allure.story("Task1=issue_2_count")
def test_login_basket(driver):
    with allure.step('Open application'):
        login = LoginBasketPage(driver)
        login.open_page()
    with allure.step('Logging into'):
        login.login_email()
        login.login_pass()
        login.login_button()
    with allure.step('Add 3 ducks into basket'):
        login.select_duck()
        login.count_duck("3")
        login.button_add_cart()
        time.sleep(2)
    with allure.step('Check of ducks which have been added is correct'):
        login.open_cart()
        assert login.count().text == "3"
        login.confirm()


@allure.story("Task1=issue_2_DB")
def test_login_basket_db(driver):
    with allure.step('Check DB'):
        db = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="",
            database="litecart"
        )
        cursor = db.cursor()
        query = "SELECT customer_firstname" \
                " payment_due FROM lc_orders ORDER BY date_created" \
                " DESC LIMIT 1"
        cursor.execute(query)
        assert cursor.fetchall()[0][0] == "MAX"
        cursor.execute(query)
        print(cursor.fetchall())
        db.close()
